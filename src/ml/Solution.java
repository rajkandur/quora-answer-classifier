package ml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import data.DataParserLoader;

import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.AbstractFileLoader;
import weka.core.converters.AbstractFileSaver;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;

public class Solution {

	private String m_arffPath = null;
	private Instances m_ins = null;
	private Instances m_testins = null;
	AttributeSelection m_filter = null;
	private J48 m_dtree = null;
	DataParserLoader m_loader = null; 

	public Solution() {
		m_arffPath = new String();

		//load the data parser
		m_loader = DataParserLoader.getInstance();
	}

	public String getM_arffPath() {
		return m_arffPath;
	}

	public void init(String trainfilename, String goldsetfilename){

		if(null == m_loader)
			return;

		//load the gold set before loading train and test
		m_loader.loadGoldOutput(goldsetfilename);
		String testfilename = trainfilename;
		m_loader.loadTrain(trainfilename);
		m_loader.loadTest(testfilename);
	}

	/***
	 * create arff from csv
	 * @param path
	 */
	boolean prepareData(String csvpath, String newarffpath, boolean bClassPresent){

		File file = new File(csvpath);

		//check the file existence		
		if(!file.exists()) return false;

		//get the loader for csv
		AbstractFileLoader fileloader = ConverterUtils.getLoaderForFile(file);	
		if(null == fileloader)
			return false;

		try {
			fileloader.setSource(file);
			Instances ins = fileloader.getDataSet();

			//set the class index
			if (ins.classIndex() == -1)
				ins.setClassIndex(ins.numAttributes() - 1);

			//save into arff
			saveInstances(newarffpath, ins);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	private void saveInstances(String newarffpath, Instances ins) throws IOException {

		//get the saver for arff
		AbstractFileSaver arffsaver = ConverterUtils.getSaverForExtension(".arff");
		if(null == arffsaver)
			return;

		//save into arff
		File newfile = new File(newarffpath);
		arffsaver.setFile(newfile);
		arffsaver.setInstances(ins);
		arffsaver.writeBatch();
	}

	/**
	 * train the model
	 */
	boolean buildClassifier(String arffpath){

		m_arffPath = arffpath;
		File file = new File(m_arffPath);

		//get the loader for csv
		AbstractFileLoader loader = ConverterUtils.getLoaderForFile(file);	
		if(null == loader)
			return false;

		try {

			loader.setSource(file);
			m_ins = loader.getDataSet();

			//set the class index
			if (m_ins.classIndex() == -1)
				m_ins.setClassIndex(m_ins.numAttributes() - 1);

			String[] options = new String[1];
			options[0] = "-U"; //unpruned tree

			//there are no instances
			if(null == m_ins)
				return false;

			//attribute selection 
			m_filter = new AttributeSelection();  // package weka.filters.supervised.attribute!
			CfsSubsetEval eval = new CfsSubsetEval();
			GreedyStepwise search = new GreedyStepwise();
			search.setSearchBackwards(true);
			m_filter.setEvaluator(eval);
			m_filter.setSearch(search);
			m_filter.setInputFormat(m_ins);
			Instances newtrainData = Filter.useFilter(m_ins, m_filter);

			//save new instances
			saveInstances("./newtrain.arff", newtrainData);

			//and create a decision tree model
			m_dtree = new J48();
			m_dtree.setOptions(options);
			m_dtree.buildClassifier(newtrainData);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	boolean classifyTestInstances(){

		if(null == m_loader)
			return false;

		Map<String, String> testsetMap = m_loader.gettestdatalistMap();
		Map<String, String> goldsetMap = m_loader.getgoldsetMap();
		//get the answer-identifiers
		Set<String> ansid = goldsetMap.keySet();

		try {
			
			System.out.print("Answer Identifier");
			System.out.print("\t");
			System.out.print("Actual Class");
			System.out.print("\t");
			System.out.println("Classified Class");
			
			//build the instances
			int correctCount = 0;
			for(String id : ansid){
				if(testsetMap.containsKey(id)){
					
					//create instance
					String atts = testsetMap.get(id);
					AbstractFileLoader loader = ConverterUtils.getLoaderForExtension(".dat");
					loader.setSource(new ByteArrayInputStream(atts.getBytes()));

					//attribute selection 
					Instances filteredins = Filter.useFilter(loader.getDataSet(), m_filter); 
					
					if (filteredins.classIndex() == -1)
						filteredins.setClassIndex(m_ins.numAttributes() - 1);

					//classify instance
					int classindex = (int)m_dtree.classifyInstance(filteredins.instance(0));
					String classifiedClass = filteredins.classAttribute().value(classindex).trim();
					System.out.print("\t");
					System.out.print(id);
					System.out.print("\t\t\t");
					System.out.print(goldsetMap.get(id));
					System.out.print("\t\t");
					System.out.println(classifiedClass);
					
					//correct count
					if(classifiedClass.equals(goldsetMap.get(id).trim()))
						correctCount++;
				}
			}
			
			//print accuracy, precision and recall
			System.out.print("Accuracy: \t");
			System.out.println((correctCount/(float)goldsetMap.size())*100);
			System.out.println(m_dtree.toSummaryString());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}
	
	boolean classifyTestInstances(String testarffpath){

		if(null == testarffpath || testarffpath.isEmpty())
			return false;

		File file = new File(testarffpath);

		//get the loader for csv
		AbstractFileLoader loader = ConverterUtils.getLoaderForFile(file);	
		if(null == loader || null == m_dtree)
			return false;

		try {
			loader.setSource(file);
			m_testins = loader.getDataSet();

			//set the class index
			if (m_testins.classIndex() == -1)
				m_testins.setClassIndex(m_ins.numAttributes() - 1);

			String[] options = new String[1];
			options[0] = "-U"; //unpruned tree

			//there are no instances
			if(null == m_testins)
				return false;

			//attribute selection 
			Instances newtestData = Filter.useFilter(m_testins, m_filter);

			//save new instances
			saveInstances("./newtest.arff", newtestData);

			//classify instances
			for (int i = 0; i < newtestData.numInstances(); i++) {
				int classindex = (int)m_dtree.classifyInstance(newtestData.instance(i));
				System.out.println(newtestData.classAttribute().value(classindex));
			} 

		}catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Validating the gold standards
	 * Note: The classifier should not be trained while calling cross-validation. (here am using classifier string)
	 */
	void crossfoldvalidate(){
		try {

			if(null == m_ins)
				System.out.println("Load the model before doing cross-validation.");

			String[] options = new String[1];
			options[0] = "-U"; //unpruned tree

			Evaluation eval = new Evaluation(m_ins);
			eval.crossValidateModel(new J48(), m_ins, 10, new Random(1));
			System.out.println(eval.toSummaryString("\nResults\n======\n", true));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//attribute select, train and, classify the data
		Solution c = new Solution();
		c.init("./data/input00.txt", "./data/output00.txt");
		c.prepareData("./train.dat", "./train.arff", true); 
		c.prepareData("./test.dat", "./test.arff", false); 
		c.buildClassifier("./train.arff");
		c.classifyTestInstances();
		
		//c.classifyTestInstances("./test.arff");
		//c.crossfoldvalidate();
	}
}
