package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class DataParserLoader {

	private Map<String, String> m_traindatalistMap;
	private Map<String, String> m_testdatalistMap;
	private Map<String, String> m_goldsetMap;
	private int m_numberOfAtt;

	private static DataParserLoader m_DataParserIns = null;
	public static DataParserLoader getInstance(){
		if(m_DataParserIns == null)
			m_DataParserIns = new DataParserLoader();
		return m_DataParserIns;
	}

	public Map<String, String> gettestdatalistMap() {
		return m_testdatalistMap;
	}

	public Map<String, String> gettraindatalistMap() {
		return m_traindatalistMap;
	}
	
	public Map<String, String> getgoldsetMap() {
		return m_goldsetMap;
	}

	public int getM_numberOfAtt() {
		return m_numberOfAtt;
	}

	public DataParserLoader() {
		m_traindatalistMap = new LinkedHashMap<String, String>();
		m_testdatalistMap = new LinkedHashMap<String, String>();
		m_goldsetMap = new HashMap<String, String>();
	}

	public void loadTrain(String filename){

		//store traincount and number of attributes
		String text = parseInput(1, 1, filename).get(0);
		int trainCount = Integer.valueOf(text.split(" ")[0].trim());
		m_numberOfAtt = Integer.valueOf(text.split(" ")[1].trim());

		//store the train data
		List<String> traindatalist = parseInput(2, trainCount, filename);
		for (String data : traindatalist) {
			List<String> ins = Arrays.asList(data.split(" "));
			m_traindatalistMap.put(ins.get(0), StringUtils.join(ins.subList(1, ins.size()-1), " "));
		}
		
		//write into file
		writeFile(m_traindatalistMap.values(), "train.dat");
	}

	public void loadTest(String filename){
		//store traincount and number of attributes
		String text = parseInput(m_traindatalistMap.size()+2, 1, filename).get(0);
		int testCount = Integer.valueOf(text.split(" ")[0].trim());

		//store the train data
		List<String> testdatalist = parseInput(m_traindatalistMap.size()+3, testCount, filename);
		String outc = new String();
		for (String data : testdatalist) {
			List<String> ins = Arrays.asList(data.split(" "));
			
			//get the class label
			if(m_goldsetMap.containsKey(ins.get(0)))
				outc = m_goldsetMap.get(ins.get(0));
			
			m_testdatalistMap.put(ins.get(0), outc + " " + StringUtils.join(ins.subList(1, ins.size()-1), " "));
		}
		
		//write into file
		writeFile(m_testdatalistMap.values(), "test.dat");
	}

	public void loadGoldOutput(String filename){
		List<String> golddata = parseInput(1, 9999, filename);
		
		//set the gold set map
		for(String data : golddata){
			m_goldsetMap.put(data.split(" ")[0].trim(), data.split(" ")[1].trim());
		}
	}

	List<String> parseInput(int start, int lineCount, String filename){
		List<String> dataList = new ArrayList<String>();
		try {
			FileReader freader = new FileReader(filename);
			BufferedReader breader = new BufferedReader(freader);
			String ltext = null;
			int scount = 1;
			while( ((ltext = breader.readLine()) != null) && dataList.size() < lineCount){
				if( scount++ < start)
					continue;

				dataList.add(ltext);
			}

			//close the handle
			breader.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dataList;
	}

	void writeFile(Collection<String> collection, String filename){
		File file = new File(filename);

		try {
			FileWriter writer = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(writer);
			for (String data : collection) {
				bw.write(data);
				bw.write("\n");
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
