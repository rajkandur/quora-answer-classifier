### Quora Answer Classifier Project

#### Packages
* _ml:_ To build the classifier and classify
* _data:_ Data parser and loader

#### Librariess
* weka.jar : version 3.6.9
* commons-lang3-3.1.jar: Apache commons lang

#### Run Class
* ````ml.Solution.Main.Class```

#### Algorithm description
I initially started without preprocessing of data and used supervised algorithms such as ```SVMlight, Logistic Regression, Naive Bayes and J48(Decision tree weka)``` and ended up getting accuracy around __51%.__ Then I did analysis on selection of only important attributes. I used ```greedystepwise search attribute selection approach``` in weka and shortlisted 8 out of 23 attributes such as: ```2, 7, 8, 10, 11, 15, 18, 20```. And then I tried with the same classifiers as mentioned above. J48, weka version of Decision Tree, yielded good accuracy of __82.2%__. The tree built is intuitive and is generic.